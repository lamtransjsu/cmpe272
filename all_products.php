<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Awesome Buy</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<div class="container">
    <div class="row">
        <?php
        include 'connect/connect.php';

        $sql = "SELECT id, name, description, image_url, price FROM product";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {

                echo $row["id"];
                echo ("~!");
                echo $row["name"];
                echo ("~!");
                echo  $row["image_url"];
                echo ("~!");
                echo $row["description"];
                echo ("~!");
                echo $row["price"];
                echo ("\n");
            }
        }

        $conn->close();
        ?>
    </div>
</div>

</body>
</html>
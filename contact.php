<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Contact</title>
</head>
<body>
<nav>
    <a href="index.php">Home | </a>
    <a href="about.php">About | </a>
    <a href="products.php">Products | </a>
    <a href="news.php">News | </a>
    <a href="contact.php">Contacts </a>
    <a href="login.html">Login | </a>
    <a href="users.php">Users</a>
</nav>

<h3> Contact List</h3>

<div class="container">
    <?php

    $contact_list = file('data/contact.txt');
    echo '<div class="row">';

    foreach ($contact_list as $index => $contact) {
        list($id, $first_name, $last_name, $gender, $active, $join_date, $tel, $email, $address, $image_url) = explode(",", $contact);

        echo '<div class="col-sm-4 contact-item">';
        echo '<span class="h3">' . $first_name . '</span> <span class="h3">' . $last_name . '</span>';
        echo '<h4>' . $tel . '</h4>';
        echo '<h4>' . $email . '</h4>';
        echo '<h4>' . $address . '</h4>';
        echo '</div>';
    }

    echo '</div>';
    ?>

</div>

</body>
</html>
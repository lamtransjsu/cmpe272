$(document).ready(function () {
    $('#register_user').hide();
    $('#search_user').hide();

    $('#register_user_btn').click(function () {
        $('#search_tab').hide();
        $('#register_user').show();
    });

    $('#search_btn').click(function (e) {
        $('#search_user').show();
        $('#register_user').hide();
    });

});


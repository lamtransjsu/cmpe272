<?php include 'connect/connect.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <title>Users</title>
</head>
<body>

<nav>
    <a href="index.php">Home | </a>
    <a href="about.php">About | </a>
    <a href="products.php">Products | </a>
    <a href="news.php">News | </a>
    <a href="contact.php">Contacts | </a>
    <a href="login.html">Login | </a>
    <a href="users.php">Users</a>
</nav>

<div class="row">
    <h1 class="col-xs-10 col-xs-offset-1 header"> Users</h1>
</div>
<hr/>
<div class="container">
    <div class="row">

        <div id="search_user" class="row">
            <?php

            $sql = "SELECT first_name, last_name, username, password, email, address, cell_phone, home_phone
                    FROM user
                    WHERE first_name LIKE \"%" . $_GET['search'] ."%\"
                        OR last_name LIKE \"%". $_GET['search'] . "%\"
                        OR email LIKE \"%". $_GET['search'] . "%\"
                        OR cell_phone LIKE \"%". $_GET['search'] . "%\"
                        OR home_phone LIKE \"%". $_GET['search'] . "%\"
                        ";

            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {

                    $first_name = $row["first_name"];
                    $last_name = $row["last_name"];
                    $tel = $row["cell_phone"];
                    $email = $row["email"];
                    $address = $row["address"];
                    $home_phone = $row["home_phone"];

                    echo("
			<div class=\"col-sx-6 col-md-4 \">
					<div class=\"media\">
						<div class=\"media-body\">
							<h4 class=\"media-heading\">" . $first_name . " " . $last_name . "</h4>
							<h5>" . $email . "</h5>
							<h5>" . $address . "</h5>
							<h5> CellPhone " . $tel . "</h5>
							<h5> HomePhone " . $home_phone . "</h5>
						</div></div></div>
					");
                }
            }
            ?>
        </div>
    </div>
</div>
</body>
</html>
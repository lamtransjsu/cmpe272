<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>The Awesome Company</title>
</head>

<body>
<h2> Login </h2>
<?php
extract($_POST);

if (!$USERNAME || !$PASSWORD) {
    fieldsBlank();
    die();
}

if (isset($NewUser)) {
    if (!($file = fopen("data/login.txt",
        "a"))
    ) {
        print("<title>Error</title></head><body>
                    Could not open password file
                    </body></html>");
        die();
    }

    fputs($file, "$USERNAME,$PASSWORD\n");
    userAdded($USERNAME);
} else {
    if (!($file = fopen("data/login.txt",
        "r"))
    ) {
        print("<title>Error</title></head>
                     <body>Could not open password file
                     </body></html>");
        die();
    }
    $userVerified = 0;

    while (!feof($file) && !$userVerified) {

        $line = fgets($file, 255);

        $line = chop($line);

        $field = split(",", $line, 2);

        // verify username
        if ($USERNAME == $field[0]) {
            $userVerified = 1;
            if (checkPassword($PASSWORD, $field)
                == true
            )
                accessGranted($USERNAME);
            else
                wrongPassword();

        }
    }

    fclose($file);

    if (!$userVerified)
        accessDenied();
}

function checkPassword($userpassword, $filedata)
{
    if ($userpassword == $filedata[1])
        return true;
    else
        return false;
}

function userAdded($name)
{
    $_SESSION['username'] = $name;
    print("
            <title>Secret Users</title></head>
            <body>
            <strong>This is the list of secret user: </strong>
            <ul>
                <li> Mary Smith</li>
                <li> John Wang</li>
                <li> Alex Bington </li>
            </ul>
    ");
}

function accessGranted($name)
{
    $_SESSION['username'] = $name;
    print("
            <title>Secret Users</title></head>
            <body>
            <strong>This is the list of secret user: </strong>
            <ul>
                <li> Mary Smith</li>
                <li> John Wang</li>
                <li> Alex Bington </li>
            </ul>
    ");
}

function wrongPassword()
{
    print("<title>Access Denied</title></head>
                 <body style = \"font-family: arial; 
                 font-size: 1em; color: red\">
                 <strong>You entered an invalid 
                 password.<br />Access has 
                 been denied.</strong>");
}

function accessDenied()
{
    print("<title>Access Denied</title></head>
                 <body style = \"font-family: arial; 
                 font-size: 1em; color: red\">
                 <strong>
                 You were denied access to this server.
                 <br /></strong>");
}

function fieldsBlank()
{
    print("<title>Access Denied</title></head>
                 <body style = \"font-family: arial; 
                 font-size: 1em; color: red\">
                 <strong>
                 Please fill in all form fields.
                 <br /></strong>");
}

?>

</body>
</html>
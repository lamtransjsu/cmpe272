<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Partner Users</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
<div class="container">
    <div class="row text-center">

        <h2 class="text-center"> Users from all partner companies </h2>
        <hr>

        <?php
        $web = curl_init("http://www.theblingbling.us/CMPE272/lab11122016/CURL/alluserstext.php");

        curl_setopt($web, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($web);

        $start_body = strpos($content, "<body>");
        $end_body = strpos($content, "</body>");

        $content = substr($content, $start_body, $end_body - $start_body);

        curl_close($web);

        $users = explode("\n", $content);

        foreach ($users as $index => $line) {
            if ($index == 0 || $index >= count($users) - 2) {
                continue;
            }

            list($first_name, $last_name, $email, $home_address, $home_city, $home_state, $home_zip, $home_phone, $cell_phone) = explode(",", $line);

            echo("<div class=\"col-sx-6 col-md-4 \">
        					<div class=\"media\">
        						<div class=\"media-body\">
        							<h4 class=\"media-heading\">" . $first_name . " " . $last_name . "</h4>
        							<h5>" . $email . "</h5>
        							<h5>" . $home_address . ', ' . $home_city . ', ' . $home_state . ', ' . $home_zip . "</h5>
        							<h5> CellPhone " . $cell_phone . "</h5>
        							<h5> HomePhone " . $home_phone . "</h5>
        						</div></div></div>
        					");
        }
        ?>
    </div>
</div>

</body>
</html>
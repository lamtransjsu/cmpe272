<?php include 'connect/connect.php'; ?>
<?php
$product_id = (int)$_GET['id'];

if (!isset($_COOKIE[$cookie_recent_visited])) {
    $recent_visited_arr = array($product_id);
} else {
    $recent_visited_arr = json_decode($_COOKIE[$cookie_recent_visited], false);

    $index = array_search($product_id, $recent_visited_arr, true);

    if ($index !== false && $index >= 0) {
        // remove the already existed product id
        unset($recent_visited_arr[$index]);

    } else if (count($recent_visited_arr) >= 5) {
        array_pop($recent_visited_arr);
    }

    // add product id to the beginning of the array
    array_unshift($recent_visited_arr, $product_id);
}

setcookie($cookie_recent_visited, json_encode($recent_visited_arr), time() + (86400 * 30)); // 86400 = 1 day

$product_id_str = 'i_' . $product_id;
if (!isset($_COOKIE[$cookie_most_visited])) {
    $most_visited_arr = array($product_id_str => 1);
} else {
    $most_visited_arr = json_decode($_COOKIE[$cookie_most_visited], true);

    if (isset($most_visited_arr[$product_id_str])) {
        $most_visited_arr[$product_id_str]++;
    } else {
        $temps_arr = array($product_id_str => 1);
        $most_visited_arr = array_merge($most_visited_arr, $temps_arr);
    }
}

setcookie($cookie_most_visited, json_encode($most_visited_arr, JSON_FORCE_OBJECT), time() + (86400 * 30)); // 86400 = 1 day
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Products</title>
</head>
<body>
<nav>
    <a href="index.php">Home | </a>
    <a href="about.php">About | </a>
    <a href="products.php">Products | </a>
    <a href="news.php">News | </a>
    <a href="contact.php">Contacts | </a>
    <a href="login.html">Login | </a>
    <a href="users.php">Users</a>
</nav>

<hr>
<h2 class="text-center">Product</h2>
<hr>

<div class="container">
    <div class="row text-center">

        <?php
        // Lam's product
        $sql = "SELECT p.id, name, description, image_url, date_publish, price, item_count FROM product p WHERE id = " . $product_id;
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
        if ($row = $result->fetch_assoc()) {

        $name = $row["name"];
        $prod_id = $row["id"];
        $description = $row["description"];
        $image_url = $row["image_url"];
        $publish_date = $row["date_publish"];
        $item_count = $row["item_count"];
        $price = $row["price"];
        ?>

        <div class="col-md-4 col-md-offset-4 img-container ">
            <a href="product_detail.php?id=<?php echo $prod_id; ?>">

                <?php
                echo("
               
                <div class=\"thumbnail\"> <img src=\" " . $image_url . " \" alt=\"Thumbnail Image 1\" class=\"img-responsive\" width=\"200\" height=\"200\">
					   <div class=\"caption\">
						<h3 class=\"ellipsis\">" . $name . "</h3>
						<p class=\"ellipsis\">" . $description . "</p>
						<p>$" . $price . "</p>
						<p> Number of Item Available: " . intval($item_count) . "</p>");

                echo("
						</div></div>
						</a></div>
						");
                }

                }
                ?>
        </div>
    </div>

</body>
</html>
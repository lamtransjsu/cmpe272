<?php include 'connect/connect.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Products</title>
</head>
<body>
<nav>
    <a href="index.php">Home | </a>
    <a href="about.php">About | </a>
    <a href="products.php">Products | </a>
    <a href="news.php">News | </a>
    <a href="contact.php">Contacts | </a>
    <a href="login.html">Login | </a>
    <a href="users.php">Users</a>
</nav>


<hr>
<h2 class="text-center">All Products</h2>
<hr>
<a href="products_with_filter.php?type=0">Recent products</a>
<br/>
<a href="products_with_filter.php?type=1">Most visited products</a>

<div class="container">
    <div class="row text-center">

        <?php
        // Lam's product
        $sql = "SELECT p.id, name, description, image_url, date_publish, price, item_count FROM product p";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {

        $name = $row["name"];
        $prod_id = $row["id"];
        $description = $row["description"];
        $image_url = $row["image_url"];
        $publish_date = $row["date_publish"];
        $item_count = $row["item_count"];
        $price = $row["price"];
        ?>

        <div class="col-md-4 img-container ">
            <a href="product_detail.php?id=<?php echo $prod_id; ?>">

                <?php
                echo("
               
                <div class=\"thumbnail\"> <img src=\" " . $image_url . " \" alt=\"Thumbnail Image 1\" class=\"img-responsive\" width=\"200\" height=\"200\">
					   <div class=\"caption\">
						<h3 class=\"ellipsis\">" . $name . "</h3>
						<p class=\"ellipsis\">" . $description . "</p>
						<p>$" . $price . "</p>
						<p> Number of Item Available: " . intval($item_count) . "</p>");

                echo("
						</div></div>
						</a></div>
						");
                }

                }
                ?>
        </div>
    </div>

</body>
</html>
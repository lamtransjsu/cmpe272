<?php include 'connect/connect.php'; ?>
<?php
$type = $_GET['type'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Products</title>
</head>
<body>
<nav>
    <a href="index.php">Home | </a>
    <a href="about.php">About | </a>
    <a href="products.php">Products | </a>
    <a href="news.php">News | </a>
    <a href="contact.php">Contacts | </a>
    <a href="login.html">Login | </a>
    <a href="users.php">Users</a>
</nav>


<hr>
<h2 class="text-center"><?php
    if ($type == 0) {
        echo('Recent products');
        $prod_arr = json_decode($_COOKIE[$cookie_recent_visited], false);
        $prod_str = implode(',', $prod_arr);
    } else {
        echo('Most visited products');
        $prod_arr = json_decode($_COOKIE[$cookie_most_visited], true);
        arsort($prod_arr);
        $prod_arr = array_slice($prod_arr, 0, 5, true);

        foreach ($prod_arr as $index => $value) {
            $prod_str .= substr($index, 2) . ',';
        }
        $prod_str = substr($prod_str, 0, -1);
    }

    $prod_str = '(' . $prod_str . ')';
    ?>


</h2>
<hr>

<div class="container">
    <div class="row text-center">

        <?php
        // Lam's product
        $sql = "SELECT p.id, name, description, image_url, date_publish, price, item_count FROM product p WHERE id IN " . $prod_str;
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {

        $name = $row["name"];
        $prod_id = $row["id"];
        $description = $row["description"];
        $image_url = $row["image_url"];
        $publish_date = $row["date_publish"];
        $item_count = $row["item_count"];
        $price = $row["price"];
        ?>

        <div class="col-md-4 img-container ">
            <a href="product_detail.php?id=<?php echo $prod_id; ?>">

                <?php
                echo("
               
                <div class=\"thumbnail\"> <img src=\" " . $image_url . " \" alt=\"Thumbnail Image 1\" class=\"img-responsive\" width=\"200\" height=\"200\">
					   <div class=\"caption\">
						<h3 class=\"ellipsis\">" . $name . "</h3>
						<p class=\"ellipsis\">" . $description . "</p>
						<p>$" . $price . "</p>
						<p> Number of Item Available: " . intval($item_count) . "</p>");

                echo("
						</div></div>
						</a></div>
						");
                }

                }
                ?>
        </div>
    </div>

</body>
</html>
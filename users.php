<?php include 'connect/connect.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
    <title>Users</title>
</head>
<body>

<nav>
    <a href="index.php">Home | </a>
    <a href="about.php">About | </a>
    <a href="products.php">Products | </a>
    <a href="news.php">News | </a>
    <a href="contact.php">Contacts | </a>
    <a href="login.html">Login | </a>
    <a href="users.php">Users</a>
</nav>

<div class="row">
    <h1 class="col-xs-10 col-xs-offset-1 header"> Users</h1>
</div>
<hr/>

<div class="row" id="search_tab">
    <div>
        <form method="GET" action="list_users.php">
            <div class="col-xs-10 col-xs-offset-1">
                <input class="form-control search_box" type="text" name="search" placeholder="Search User here">
            </div>

            <div class="col-xs-4 col-xs-offset-4">
                <button id="search_btn" type="submit" class="btn btn-primary btn-block">Search</button>
            </div>
        </form>
    </div>
    <div class="col-xs-4 col-xs-offset-4">
        <a href="#" class="register_text" id="register_user_btn"> Register New User </a>
    </div>
    <hr/>
</div>

<div id="register_user" class="row">

    <form method="POST" action="user_create.php" class="form-input-md">
        <input type="text" required class="col-xs-8 col-xs-offset-2 input-lg" name="user_first_name"
               placeholder="First Name">
        <input type="text" required class="col-xs-8 col-xs-offset-2 input-lg" name="user_last_name"
               placeholder="Last Name">
        <input type="text" required class="col-xs-8 col-xs-offset-2 input-lg" name="username" placeholder="Username">
        <input type="password" required class="col-xs-8 col-xs-offset-2 input-lg" name="password"
               placeholder="Password">
        <input type="email" required class="col-xs-8 col-xs-offset-2 input-lg" name="user_email" placeholder="Email">
        <input type="text" required class="col-xs-8 col-xs-offset-2 input-lg" name="user_address" placeholder="Address">
        <input type="tel" required class="col-xs-8 col-xs-offset-2 input-lg" name="user_cellphone"
               placeholder="Cellphone">
        <input type="tel" required class="col-xs-8 col-xs-offset-2 input-lg" name="user_homephone"
               placeholder="Homephone">

        <div class="col-xs-4 col-xs-offset-2">
            <label class="radio-inline form-radio-lg"><input type="radio" name="gender" value="0">Male</label>
            <label class="radio-inline form-radio-lg"><input type="radio" name="gender" value="1">Female</label>
        </div>

        <button type="submit" class="col-xs-2 col-xs-offset-5 btn btn-primary">REGISTER</button>
    </form>
</div>

</body>
</html>